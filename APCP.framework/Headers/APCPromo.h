//
//  CPromo.h
//  CrossPromoExample
//
//  Created by Appxplore 14 on 1/12/17.
//  Copyright © 2017 Appxplore Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, APCPMoreGameCachedError)
{
    kMGSuccess,
    kMGNotInit,
    kMGCacheRequestError,
    kMGCacheError
};

typedef NS_ENUM(NSInteger, kAPCPViewAnimation)
{
    kViewAnimationSlideFromBottom = 0,
    kViewAnimationSlideFromTop = 1,
    kViewAnimationSlideFromLeft = 2,
    kViewAnimationSlideFromRight = 3,
};

NS_ASSUME_NONNULL_BEGIN
@protocol APCPDelegate <NSObject>

- (void)APCPInitDone;
- (void)APCPInitError:(NSString *) errorMsg;

- (void)APCPMoreGamesClosed;
- (void)APCPMoreGamesCachedReady;
- (void)APCPMoreGamesCachedError:(NSString *) errorMsg;

- (void)APCPInterstitialClosed:(NSString *)locationKey;
- (void)APCPInterstitialCachedReady:(NSString *)locationKey;
- (void)APCPInterstitialCachedError:(NSString *)locationKey ErrorMsg:(NSString *) errorMsg;
- (void)APCPConsentDialogResult:(BOOL) consentGiven;
@end


@interface APCPromo : NSObject

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)initialize NS_UNAVAILABLE;

+ (void)initialize:(NSString *)gameId delegate:(nullable id<APCPDelegate>)delegate testMode:(BOOL) isTestMode;
+ (id<APCPDelegate>)getDelegate;
+ (void)connect;
+ (void)setCustomId:(NSString *)customId;
+ (void)setCustomLang:(NSString *)customLang;


+ (BOOL)isInitialized;

+ (void)setAutoCacheMoreGameOnStartOnly:(BOOL) isCache;
+ (void)setAutoCacheMoreGame:(BOOL) autocache;
+ (void)setMoreGameAnimationType:(kAPCPViewAnimation) animType;
+ (BOOL)isMoreGameReady;
+ (void)cacheMoreGames;
+ (BOOL)showMoreGames:(UIViewController *)rootView;

+ (void)setAutoCacheInterstitial:(BOOL) autocache;
+ (void)setInterstitialAnimationType:(kAPCPViewAnimation) animType;
+ (void)setInterstitialLocationList:(NSArray *)listOfInterstitialLocations;
+ (BOOL)isInterstitialReady:(NSString *)locationKey;
+ (void)cacheInterstitial:(NSString *)locationKey;
+ (void)cacheAllInterstitials;
+ (BOOL)showInterstitial:(NSString *)locationKey RootView:(UIViewController *)rootView;

+ (void) setDeepLinkData:(NSURL *) url;
+ (NSString *) getDeepLinkData;
+ (void) removeDeepLinkData;

+ (void) setConsent:(BOOL) consentGiven;
+ (BOOL) isConsentGiven;
+ (void) showConsentDialog: (NSString *) privacyPolicyUrl RootView:(UIViewController *)rootView;

@end

NS_ASSUME_NONNULL_END
